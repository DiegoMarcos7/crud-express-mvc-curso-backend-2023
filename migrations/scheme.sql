USE mysql;
DROP DATABASE IF EXISTS reservation_db ;
CREATE DATABASE reservation_db;

USE reservation_db;

/*SCHEME*/
CREATE TABLE role (
    id TINYINT NOT NULL,
    name VARCHAR(20) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE users(
    id INT NOT NULL AUTO_INCREMENT,
    username VARCHAR(15) NOT NULL,
    password VARCHAR(60) NOT NULL,
    role_id TINYINT NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (role_id) REFERENCES role(id)
);

CREATE TABLE country (
    id VARCHAR(2) NOT NULL,
    name VARCHAR(15) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE customer (
    id INT NOT NULL,
    name VARCHAR(50) NOT NULL,
    surname VARCHAR(50) NOT NULL,
    email VARCHAR(50) NOT NULL,
    phone_number char(9) NOT NULL,
    country_id VARCHAR(2) NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (id) REFERENCES users(id),
    FOREIGN KEY (country_id) REFERENCES country(id)
);

CREATE TABLE reservation (
    id INT NOT NULL AUTO_INCREMENT,
    customer_id INT NOT NULL,
    check_in datetime NOT NULL,
    check_out datetime NOT NULL,
    reservation_date datetime NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (customer_id) REFERENCES customer(id)
);

CREATE TABLE room_type (
    id TINYINT NOT NULL,
    name VARCHAR(50) NOT NULL,
    capacity INT NOT NULL,
    nightly_price DECIMAL(5,2) NOT NULL,
    characteristics VARCHAR(100) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE room (
    id CHAR(2) NOT NULL,
    room_type_id TINYINT NOT NULL,
    status BIT NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (room_type_id) REFERENCES room_type(id)
);

CREATE TABLE reservation_room (
    id INT NOT NULL AUTO_INCREMENT,
    room_id CHAR(2) NOT NULL,
    reservation_id INT NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (room_id) REFERENCES room(id),
    FOREIGN KEY (reservation_id) REFERENCES reservation(id) ON DELETE CASCADE
);

CREATE TABLE payment_method (
    id TINYINT NOT NULL,
    name VARCHAR(15) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE payment (
    id INT NOT NULL AUTO_INCREMENT,
    reservation_id INT NOT NULL,
    payment_method_id TINYINT NOT NULL,
    payment_date datetime NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (reservation_id) REFERENCES reservation(id) ON DELETE CASCADE,
    FOREIGN KEY (payment_method_id) REFERENCES payment_method(id)
);

/*DATA*/
INSERT INTO role (id, name) VALUES
    (1, 'Customer');


-- ENCRYPT-PEPPER -> TU-HOTEL-M
INSERT INTO users (id, username, password, role_id) VALUES
    (1, 'jperez', '$2a$12$4mMeF1IqCcn5js8yDMxY7uHmMT0P5GfYE5/lOe.SnduwRs9VNnyhu', 1),
    (2, 'agomez', '$2a$12$mfrwxVFmPJPJHMuC0Ce7XeESo0YEU7eOxEMfAaqezLGp/Yl9erJaC', 1),
    (3, 'msmith', '$2a$12$bVcrJ1ySevxPX35jAKT2duM/googLyLtB3H6pFEI3btDQZbiHdjcW', 1),
    (4, 'mrodriguez', '$2a$12$OdEhBxEwDIAQHxP5ehLLXuq.v/jVihwUtt/H0BNEqxKMfJjWVkRS2', 1),
    (5, 'ebrown', '$2a$12$ct11vHa7CCP3Qbwv9fWQSuH7WHbL7J8HA9.E0bEdnelbrt75bVsb.', 1);

INSERT INTO country (id, name)
VALUES
    ('PE', 'Perú'),     
    ('US', 'Estados Unidos'),  
    ('BR', 'Brasil'),    
    ('AR', 'Argentina'), 
    ('CL', 'Chile'),     
    ('CO', 'Colombia'),  
    ('EC', 'Ecuador'),   
    ('ES', 'España'),    
    ('FR', 'Francia'),   
    ('IT', 'Italia'),    
    ('DE', 'Alemania'),  
    ('UK', 'Reino Unido'); 

INSERT INTO customer (id, name, surname, email, phone_number, country_id)
VALUES
    (1,'Juan', 'Pérez', 'juan.perez@example.com', '923456789', 'PE'),
    (2,'Ana', 'Gómez', 'ana.gomez@example.com', '976543210', 'ES'),
    (3,'Michael', 'Smith', 'michael.smith@example.com', '345678901', 'US'),
    (4,'Maria', 'Rodriguez', 'maria.rodriguez@example.com', '945678901', 'PE'),
    (5,'Emily', 'Brown', 'emily.brown@example.com', '943210987', 'UK');

INSERT INTO reservation (id, customer_id, check_in, check_out, reservation_date)
VALUES
    (1, 2, '2023-10-15 14:00:00', '2023-10-18 12:00:00', '2023-09-25 10:30:00'),
    (2, 1, '2023-11-05 15:00:00', '2023-11-08 11:30:00', '2023-10-20 09:45:00'),
    (3, 5, '2023-12-10 12:00:00', '2023-12-15 10:00:00', '2023-11-15 14:20:00'),
    (4, 4, '2023-11-20 13:30:00', '2023-11-25 11:00:00', '2023-10-30 08:15:00'),
    (5, 3, '2023-12-01 16:00:00', '2023-12-05 10:30:00', '2023-11-10 11:00:00');

INSERT INTO room_type (id, name, capacity, nightly_price, characteristics)
VALUES
    (1,'Habitación Individual', 1, 80.00, 'Vista a la ciudad, Wi-Fi gratuito, baño privado'),
    (2,'Habitación Doble Estandar', 2, 120.00, '2 Camas indviduales, TV, baño privado'),
    (3,'Habitación Doble Superior', 2, 150.00, '1 Cama doble, balcón, minibar'),
    (4,'Suite Junior', 2, 180.00, 'Zona de estar, jacuzzi, desayuno incluido');

/*status - bit | 0 - no disponible | 1 - disponible*/
INSERT INTO room (id, room_type_id, status) VALUES
    ('A1', 1, 1),
    ('A2', 1, 1),
    ('A3', 1, 1),
    ('A4', 2, 0),
    ('A5', 2, 1),
    ('A6', 3, 0),
    ('B1', 1, 1),
    ('B2', 1, 1),
    ('B3', 1, 0),
    ('B4', 2, 1),
    ('B5', 3, 0),
    ('B6', 4, 1);

INSERT INTO reservation_room (id, room_id, reservation_id)
VALUES
    (1, 'A1', 1),
    (2, 'A3', 2),
    (3, 'A3', 3),
    (4, 'A5', 4),
    (5, 'B6', 5);

INSERT INTO payment_method (id, name)
VALUES
    (1, 'Yape'),
    (2, 'Tarjeta'),
    (3, 'Efectivo');

INSERT INTO payment (id, reservation_id, payment_method_id, payment_date)
VALUES
    (1, 1, 1, '2023-10-15 12:30:00'),
    (2, 2, 3, '2023-11-05 14:15:00'),
    (3, 3, 1, '2023-12-10 10:00:00'),
    (4, 4, 2, '2023-11-20 16:45:00'),
    (5, 5, 3, '2023-12-01 13:20:00');

/* Stored Procedures */
DROP PROCEDURE IF EXISTS get_all_reservations;
DELIMITER //
CREATE PROCEDURE get_all_reservations()
BEGIN
    SELECT * FROM reservation;
END //

DROP PROCEDURE IF EXISTS find_reservation_by_id;
DELIMITER //
CREATE PROCEDURE find_reservation_by_id(
    IN _reservation_id INT
)
BEGIN
    SELECT 
	r.id as "reservation_id",
    r.check_in,
    r.check_out,
    r.reservation_date,
    c.id as "customer_id",
    c.name,
    c.surname,
    c.email,
    c.phone_number,
    c.country_id
    FROM reservation r 
    INNER JOIN customer c ON r.customer_id = c.id
    WHERE r.id = _reservation_id;
END //

DROP PROCEDURE IF EXISTS create_reservation;
DELIMITER //
CREATE PROCEDURE create_reservation(
    IN _customer_id INT,
    IN _check_in DATETIME, 
    IN _check_out DATETIME,
    IN _reservation_date DATETIME
)
BEGIN
    INSERT INTO reservation(customer_id, check_in, check_out, reservation_date) 
    VALUES (_customer_id, _check_in, _check_out, _reservation_date);
END //

DROP PROCEDURE IF EXISTS update_reservation;
DELIMITER //
CREATE PROCEDURE update_reservation(
    IN _reservation_id INT,
    IN _check_in DATETIME,
    IN _check_out DATETIME
)
BEGIN
    UPDATE reservation 
    SET check_in = _check_in, check_out = _check_out WHERE id = _reservation_id;
END //

DROP PROCEDURE IF EXISTS cancel_reservation;
DELIMITER //
CREATE PROCEDURE cancel_reservation(
    IN _reservation_id INT
)
BEGIN
    DELETE FROM payment
    WHERE reservation_id = _reservation_id;
    DELETE FROM reservation
    WHERE id = _reservation_id;
END //

DROP PROCEDURE IF EXISTS get_all_room_types;
DELIMITER //
CREATE PROCEDURE get_all_room_types()
BEGIN
    SELECT * FROM room_type;
END //

DROP PROCEDURE IF EXISTS get_all_payment_methods;
DELIMITER //
CREATE PROCEDURE get_all_payment_methods()
BEGIN
    SELECT * FROM payment_method;
END //

DROP PROCEDURE IF EXISTS get_all_countries;
DELIMITER //
CREATE PROCEDURE get_all_countries()
BEGIN
    SELECT * FROM country;
END //

DROP PROCEDURE IF EXISTS get_all_available_rooms;
DELIMITER //
CREATE PROCEDURE get_all_available_rooms()
BEGIN
    SELECT 
    	r.id as 'room_id', 
    	rt.id as 'room_type_id',
        rt.name,
        rt.capacity,
        rt.nightly_price,
        rt.characteristics
    FROM room r 
    INNER JOIN room_type rt ON r.room_type_id = rt.id
    WHERE r.status = 1;
END //

DROP PROCEDURE IF EXISTS find_room_by_id;
DELIMITER //
CREATE PROCEDURE find_room_by_id(
    IN _room_id CHAR(2)
)
BEGIN
    SELECT 
    	r.id as 'room_id', 
    	rt.id as 'room_type_id',
        rt.name,
        rt.capacity,
        rt.nightly_price,
        rt.characteristics
    FROM room r 
    INNER JOIN room_type rt ON r.room_type_id = rt.id
    WHERE r.status = 1 AND r.id = _room_id;
END //

DROP PROCEDURE IF EXISTS find_reservation_payment_by_id;
DELIMITER //
CREATE PROCEDURE find_reservation_payment_by_id(
    IN _reservation_id INT
)
BEGIN
    SELECT 
		p.id as 'payment_id',
        p.reservation_id,
        pm.id as "payment_method_id",
        pm.name,       
   		p.payment_date
    FROM payment p 
    INNER JOIN payment_method pm ON p.payment_method_id = pm.id
    WHERE p.reservation_id = _reservation_id;
END //

DROP PROCEDURE IF EXISTS get_password_by_username;
DELIMITER //
CREATE PROCEDURE get_password_by_username(
    IN _username VARCHAR(15)
)
BEGIN
    SELECT u.id, u.username, u.password FROM users u
    WHERE u.username = _username;
END //

DROP PROCEDURE IF EXISTS register_customer_user;
DELIMITER //
CREATE PROCEDURE register_customer_user(
    IN _username VARCHAR(15),
    IN _password VARCHAR(60),
    IN _name VARCHAR(50) ,
    IN _surname VARCHAR(50) ,
    IN _email VARCHAR(50) ,
    IN _phone_number char(9) ,
    IN _country_id VARCHAR(2)
)
BEGIN
    INSERT INTO users(`username`, `password`, `role_id`) 
    VALUES ( _username, _password, 1);
    SET @user_id = LAST_INSERT_ID();
    INSERT INTO customer (id, name, surname, email, phone_number, country_id)
    VALUES
    (@user_id, _name, _surname, _email, _phone_number, _country_id);
END //

DROP PROCEDURE IF EXISTS get_all_customers;
DELIMITER //
CREATE PROCEDURE get_all_customers()
BEGIN
    SELECT * FROM customer;
END //

DROP PROCEDURE IF EXISTS find_customer_by_id;
DELIMITER //
CREATE PROCEDURE find_customer_by_id(
    IN _customer_id INT
)
BEGIN
    SELECT *
    FROM customers 
    WHERE id = _customer_id;
END //