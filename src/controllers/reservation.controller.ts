import Reservation, { ExtendedReservation } from "../entities/Reservation";
import ReservationService from "../services/reservation.service"
import ApiResponse from "../utils/http";
import ControllerBase from "./ControllerBase";
import ReservationCreateDTO from "./dtos/ReservationCreateDTO";
import ReservationUpdateDTO from "./dtos/ReservationUpdateDTO";

export default class ReservationController extends ControllerBase{
	private reservationService: ReservationService
	constructor() {
		super('/reservations')
		this.reservationService = new ReservationService()
		
	}
	onEndpoints(){
		this.onGetReservation()
		this.onFindReservation()
		this.onCreateReservation()
		this.onUpdateReservation()
		this.onCancelReservation()
	}
	private onGetReservation() {
		this.router.get("/", async (_, res) => {
			const reservations : Reservation[] = await this.reservationService.getReservations()
			res.json(ApiResponse.complete('SUCCESS', reservations))
		});
	}
	private onFindReservation() {
		this.router.get("/:id", async (req, res) => {
			const reservationId : number = parseInt(req.params.id)
			const foundReservation : ExtendedReservation = 
				await this.reservationService.findReservation(reservationId)
			if (foundReservation) {
				res.json(ApiResponse.complete<ExtendedReservation>('SUCCESS', foundReservation))
				// res.json(foundReservation)
				return;
			}
			res.json(ApiResponse.complete<null>('NOT_FOUND', null))
		});
	}
	private onCreateReservation() {
		this.router.post("/create", async (req, res) => {
			req.body["reservationDate"] = new Date()

			if (!ReservationCreateDTO.isValid(req.body)) {
				res.status(400).json(ApiResponse.empty())
				return
			}
			const reservationCreateDTO : ReservationCreateDTO = new ReservationCreateDTO(req.body)
			const success : boolean = 
				await this.reservationService.createReservation(reservationCreateDTO)

			if (success) {
				res.json(ApiResponse.complete<null>('SUCCESS_RESERVATION_CREATED', null))
				return
			}
			res.status(400).json(ApiResponse.empty())
		});
	}
	private onUpdateReservation() {
		this.router.patch("/change-calendar/:id", async (req, res) => {
			const reservationId : number = parseInt(req.params.id)
			if (ReservationUpdateDTO.isValid(reservationId, req.body)) {
				const reservationUpdate = new ReservationUpdateDTO(reservationId, req.body)
				const success : boolean = 
					await this.reservationService.editReservation(reservationUpdate)
				if (success) {
					res.json(ApiResponse.complete<null>("SUCCESS_RESERVATION_DATE_UPDATED", null))
					return
				}
			}
			res.status(400).json(ApiResponse.empty())
		});
	}
	private onCancelReservation() {
		this.router.delete("/cancel/:id", async (req, res) => {
			const reservationId : number = parseInt(req.params.id)
			const success :boolean = await this.reservationService.cancelReservation(reservationId)
			if (success) {
				res.json({message : "SUCCESS_RESERVATION_CANCELED", data : null})
				return
			}
			res.status(400).json(ApiResponse.empty())
		});
	}
}