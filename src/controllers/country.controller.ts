import ControllerBase from "./ControllerBase"
import CountryService from "../services/country.service"
import Country from "../entities/Country"
import ApiResponse from "../utils/http"
export default class CountryController extends ControllerBase{
    private countryService: CountryService
    constructor() {
        super('/countries')
        this.countryService = new CountryService()  
    }
    onEndpoints(){
        this.onGetCountries()
    }
    private onGetCountries() {
        this.router.get("/", async (_, res) => {
            const countries : Country[] = 
                await this.countryService.getCountries()
            res.json(ApiResponse.complete<Country[]>('SUCCESS', countries))
        })
    }
}