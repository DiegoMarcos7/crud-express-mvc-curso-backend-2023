import Payment from "../entities/Payment"
import PaymentService from "../services/payment.service"
import ApiResponse from "../utils/http"
import ControllerBase from "./ControllerBase"

export default class PaymentController extends ControllerBase{
    private paymentService: PaymentService
    constructor() {
        super('/reservation-payments')
        this.paymentService = new PaymentService()  
    }
    onEndpoints(){
        this.onFindReservationPayment()
    }
    private onFindReservationPayment() {
        this.router.get("/:id", async (req, res) => {
            const id : number = parseInt(req.params.id)
            const reservationPayment: Payment = 
                await this.paymentService.findReservationPayment(id)
            if (reservationPayment) {
                res.json(ApiResponse.complete<Payment>('SUCCESS', reservationPayment))
                return;
            }
            res.json(ApiResponse.complete<null>('NOT_FOUND', null))
        })
    }
}