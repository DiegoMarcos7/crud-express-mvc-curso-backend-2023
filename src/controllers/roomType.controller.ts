import ControllerBase from "./ControllerBase"
import RoomTypeService from "../services/roomType.service"
import RoomType from "../entities/RoomType"
import ApiResponse from "../utils/http"

export default class RoomTypeController extends ControllerBase{
        private roomTypeService: RoomTypeService
        constructor() {
            super('/room-types')
            this.roomTypeService = new RoomTypeService()
            
        }
        onEndpoints(){
            this.onGetRoomTypes()
        }
        private onGetRoomTypes() {
            this.router.get("/", async (_, res) => {
                const roomTypes : RoomType[] = await this.roomTypeService.getRoomTypes()
                res.json(ApiResponse.complete<RoomType[]>('SUCCESS', roomTypes))
            })
        }
}