import { Router } from "express"

abstract class ControllerBase {
    private _root : string
    private _router : Router
    constructor(root : string) {
        this._root = root
        this._router = Router()
    }
    get root() { return this._root}
    get router() { return this._router}
    
    abstract onEndpoints() : void
}

export default ControllerBase