import { isPassword, isUsername } from "../../utils/validators"

export default class UserRegisterDTO {
    readonly username: string
    readonly password: string
    constructor({username, password} : any) {
        this.username = username
        this.password = password
    }
    static isValid(body : any){
        return (
            isUsername(body.username) &&
            isPassword(body.password)
        )
    }
}