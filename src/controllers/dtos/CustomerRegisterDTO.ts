import { isCountryId, isEmail, isNameOrSurname, isPhoneNumber } from "../../utils/validators"

export default class CustomerRegisterDTO {
    readonly name: string
    readonly surname: string
    readonly email: string
    readonly phoneNumber: number
    readonly countryId: string
    constructor({name, surname, email, phoneNumber, countryId} : any) {
        this.name = name,
        this.surname = surname,
        this.email = email,
        this.phoneNumber = phoneNumber,
        this.countryId = countryId
    }
    static isValid(body : any){
        return (
            isNameOrSurname(body.name) &&
            isNameOrSurname(body.surname) &&
            isEmail(body.email) &&
            isPhoneNumber(body.phoneNumber) &&
            isCountryId(body.countryId)
        )
    }
}