import { isDateValid, isNumber } from "../../utils/validators"

export default class ReservationCreateDTO {
    readonly customerId: string
    readonly checkIn: Date
    readonly checkOut: Date
    readonly reservationDate: Date
    constructor({customerId, checkIn, checkOut, reservationDate} : any) {
        this.customerId = customerId
        this.checkIn = new Date(checkIn),
        this.checkOut = new Date(checkOut),
        this.reservationDate = new Date(reservationDate)
    }
    static isValid(body : any) : boolean{
        return (
            isNumber(body.customerId)&& 
            isDateValid(body.checkIn) && 
            isDateValid(body.checkOut) && 
            isDateValid(body.reservationDate)
        )
    }
}