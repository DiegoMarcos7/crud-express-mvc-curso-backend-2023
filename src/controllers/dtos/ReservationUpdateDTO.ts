import { isDateValid, isNumber } from "../../utils/validators"

export default class ReservationCreateDTO {
    readonly reservationId: number
    readonly checkIn: Date
    readonly checkOut: Date
    constructor(reservationId : number, {checkIn, checkOut} : any) {
        this.reservationId = reservationId
        this.checkIn = new Date(checkIn)
        this.checkOut = new Date(checkOut)
    }
    static isValid(reservationId : any, body : any) : boolean{
        return (
            isNumber(reservationId) &&
            isDateValid(body.checkIn) && 
            isDateValid(body.checkOut)
        )
    }
}