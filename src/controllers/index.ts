import { Application } from "express";
import ControllerBase from "./ControllerBase"
import ReservationController from "./reservation.controller"
import RoomTypeController from "./roomType.controller"
import PaymentMethodController from "./paymentMethod.controller"
import CountryController from "./country.controller"
import RoomController from "./room.controller"
import PaymentController from "./payment.controller";
import UserController from "./user.controller";
import CustomerController from "./customer.controller";

export default class ControllerInitializer {
	private app : Application
	private controllers : ControllerBase[]
	constructor(app : Application) {
		this.app = app
		this.controllers = [
			new ReservationController(),
			new RoomTypeController(),
			new PaymentMethodController(),
			new CountryController(),
			new RoomController(),
			new PaymentController(),
			new UserController(),
			new CustomerController()
		]
	}
	init() {
		this.controllers.forEach( controller =>{
			const {root , router} = controller
			controller.onEndpoints()
			this.app.use(this.getEndpoint(root), router)
		})
	}
	private getEndpoint(root : string) : string{
		return `/api${root}`
	}
}
