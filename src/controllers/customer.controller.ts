import Customer from "../entities/Customer";
import CustomerService from "../services/customer.service";
import ApiResponse from "../utils/http";
import ControllerBase from "./ControllerBase";

export default class CustomerController extends ControllerBase{
    private customerService: CustomerService
    constructor() {
        super('/customers')
        this.customerService = new CustomerService()  
    }
    onEndpoints(){
        this.onGetCustomers()
        this.onFindCustomer()
    }
    private onGetCustomers() {
        this.router.get("/", async (_, res) => {
            const customers : Customer[] = 
                await this.customerService.getCustomers()
            res.json(ApiResponse.complete<Customer[]>('SUCCESS', customers))
        })
    }
    private onFindCustomer(){
        this.router.get("/:id", async (req, res) => {
            const foundCustomer : Customer = await this.customerService.findCustomer(parseInt(req.params.id))
            if (foundCustomer) {
                res.json(ApiResponse.complete<Customer>('SUCCESS', foundCustomer))
                return
            }
            res.status(404).json(ApiResponse.complete<null>('NOT_FOUND', null))
        })
    }
}