import ControllerBase from "./ControllerBase"
import RoomService from "../services/room.service"
import Room from "../entities/Room"
import ApiResponse from "../utils/http"
export default class RoomController extends ControllerBase{
    private roomService: RoomService
    constructor() {
        super('/rooms')
        this.roomService = new RoomService()  
    }
    onEndpoints(){
        this.onGetRooms()
        this.onFindAvailableRoom()
    }
    private onGetRooms() {
        this.router.get("/available", async (_, res) => {
            const rooms : Room[] = 
                await this.roomService.getAvailableRooms()
            res.json(ApiResponse.complete<Room[]>('SUCCESS', rooms))
        })
    }
    private onFindAvailableRoom(){
        this.router.get("/:id", async (req, res) => {
            const foundRoom : Room = await this.roomService.findAvailableRoom(req.params.id)
            if (foundRoom) {
                res.json(ApiResponse.complete<Room>('SUCCESS', foundRoom))
                return
            }
            res.json(ApiResponse.complete<null>('NOT_FOUND', null))
        })
    }
}