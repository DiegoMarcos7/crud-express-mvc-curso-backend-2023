import UserService from "../services/user.service"
import ApiResponse from "../utils/http"
import ControllerBase from "./ControllerBase"
import CustomerRegisterDTO from "./dtos/CustomerRegisterDTO"
import UserRegisterDTO from "./dtos/UserRegisterDTO"

export default class UserController extends ControllerBase{
    private userService: UserService
    constructor() {
        super('/auth')
        this.userService = new UserService()  
    }
    onEndpoints(){
        this.onUserSignUp()
        this.onUserSignIn()
    }
        
    private onUserSignUp() {
        this.router.post("/sign-up", async (req, res) => {
            if (UserRegisterDTO.isValid(req.body) && CustomerRegisterDTO.isValid(req.body)) {
                const userRegisterDTO : UserRegisterDTO = new UserRegisterDTO(req.body)
                const customerRegisterDTO : CustomerRegisterDTO = new CustomerRegisterDTO(req.body)
                const success : boolean = 
                    await this.userService.registerCustomerUser(userRegisterDTO, customerRegisterDTO)
                if (success) {
                    res.json(ApiResponse.complete<null>('SUCCESS_USER_REGISTERED', null))
                    return
                }
            }
			res.status(400).json(ApiResponse.empty())
        })
    }

    private onUserSignIn() {
        this.router.post("/sign-in", async (req, res) => {
            const {username, password} = req.body
            const token : string = await this.userService.findPasswordByUsername(username, password)
            if(!token){
                res.status(401).json(ApiResponse.complete<null>('WRONG_USER_OR_PASSWORD', null))
                return
            }
            res.json(ApiResponse.complete<string>('SUCCESS_USER_AUTHENTICATED', token))  
        })
    }
}