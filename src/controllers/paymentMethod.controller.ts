import ControllerBase from "./ControllerBase"
import PaymentMethodService from "../services/paymentMethod.service"
import PaymentMethod from "../entities/PaymentMethod"
import ApiResponse from "../utils/http"
export default class PaymentMethodController extends ControllerBase{
    private paymentMethodService: PaymentMethodService
    constructor() {
        super('/payment-methods')
        this.paymentMethodService = new PaymentMethodService()  
    }
    onEndpoints(){
        this.onGetPaymentMethods()
    }
    private onGetPaymentMethods() {
        this.router.get("/", async (_, res) => {
            const paymentMethods : PaymentMethod[] = 
                await this.paymentMethodService.getPaymentMethods()
            res.json(ApiResponse.complete<PaymentMethod[]>('SUCCESS', paymentMethods))
        })
    }
}