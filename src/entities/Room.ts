import RoomType from "./RoomType"

type Room = {
    id : number, 
    roomType : RoomType   
}

export default Room