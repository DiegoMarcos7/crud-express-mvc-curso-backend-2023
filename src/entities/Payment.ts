import PaymentMethod from "./PaymentMethod"

type Payment = {
    id : number, 
    reservationId : number,
    paymentMethod : PaymentMethod, 
    paymentDate : Date
}

export default Payment