type PaymentMethod = {
    id : Number; 
    name : string;
}

export default PaymentMethod