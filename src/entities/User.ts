type User = {
    id: number,
    username: string, 
    passwordHashed?: string
}

export default User