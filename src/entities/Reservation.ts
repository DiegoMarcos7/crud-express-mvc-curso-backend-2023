import Customer from "./Customer";

type Reservation = {
    id?: number;
    customerId?: number;
    checkIn: Date;
    checkOut: Date;
    reservationDate: Date;
}

export type ExtendedReservation = Reservation & {
    customer : Customer;
}

export default Reservation;