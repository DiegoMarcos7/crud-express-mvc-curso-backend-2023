type Customer = {
    id: Number, 
    name : string,
    surname : string, 
    email : string, 
    phoneNumber : string, 
    countryId : string
}

export default Customer