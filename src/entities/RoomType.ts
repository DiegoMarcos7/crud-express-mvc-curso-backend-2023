type RoomType = {
    id : number,
    name : string,
    capacity : number,
    nightlyPrice : number,
    characteristics : string
}

export default RoomType