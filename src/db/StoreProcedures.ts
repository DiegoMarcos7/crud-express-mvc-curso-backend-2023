export enum StoreProcedures {
    getAllReservation = "CALL get_all_reservations()",
    findReservationById = `CALL find_reservation_by_id(?);`,
    createReservation = `CALL create_reservation(?, ?, ?, ?);`,
    updateReservation = `CALL update_reservation(?,?,?)`,
    cancelReservation = `CALL cancel_reservation(?)`,
    getAllRoomTypes = "CALL get_all_room_types()",
    getAllPaymentMethods = "CALL get_all_payment_methods()",
    getAllCountries = "CALL get_all_countries()",
    getAllAvailableRooms = "CALL get_all_available_rooms()",
    findRoomById = "CALL find_room_by_id(?)",
    getReservationPaymentById = "CALL find_reservation_payment_by_id(?)",
    findPasswordByUsername = "CALL get_password_by_username(?)",
    registerCustomerUser = "CALL register_customer_user(?,?,?,?,?,?,?)",
    getAllCustomers = "CALL get_all_customers()",
    findCustomerById = "CALL find_customer_by_id(?)",
}

export default StoreProcedures