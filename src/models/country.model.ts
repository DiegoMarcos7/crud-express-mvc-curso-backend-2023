import StoreProcedures from "../db/StoreProcedures"
import ModelBase from "./ModelBase"
import CountryQE from "./query-entity/CountryQE"

export default class CountryModel extends ModelBase{
	async getCountries() : Promise<CountryQE[]> {
		const [[resultset]] = await this.database.query(StoreProcedures.getAllCountries)
        return resultset.map( (record : any) => new CountryQE(record) )
	}
}