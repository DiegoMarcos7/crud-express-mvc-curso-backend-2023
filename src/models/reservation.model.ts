import ReservationCreateDTO from "../controllers/dtos/ReservationCreateDTO"
import ReservationUpdateDTO from "../controllers/dtos/ReservationUpdateDTO"
import StoreProcedures from "../db/StoreProcedures"
import Reservation, { ExtendedReservation } from "../entities/Reservation"
import ModelBase from "./ModelBase"
import extendedReservationMapper from "./mappers/extendedReservation.mapper"
import ReservationQE from "./query-entity/ReservationQE"

export default class ReservationModel extends ModelBase {
	async getReservations() : Promise<ReservationQE[]> {
		const [[resultset]] = 
			await this.database.query(StoreProcedures.getAllReservation) as [any[]]
			
		return resultset.map( (record : any) => new ReservationQE(record) )
	}
	async findReservation(id : number) : Promise<ExtendedReservation> {
		// -> [[]] - accedemos al primer item del primer item
		const [[resultset]] = await this.database.query(StoreProcedures.findReservationById, [id])
		if(!Array.isArray(resultset) || resultset.length === 0) return null
		return extendedReservationMapper(resultset)
	}
	async createReservation(reservation : ReservationCreateDTO) : Promise<number>{
		const [information] = await this.database.query(
			StoreProcedures.createReservation, 
			[reservation.customerId, reservation.checkIn, reservation.checkOut, reservation.reservationDate])
		return information.affectedRows
	}
	async editReservation({reservationId, checkIn, checkOut} : ReservationUpdateDTO) : Promise<number>{
		const [information] = await this.database.query(StoreProcedures.updateReservation, [reservationId, checkIn,checkOut])
		return information.affectedRows
	}
	async cancelReservation(reservationId : number) : Promise<number>{
		const [information] = await this.database.query(StoreProcedures.cancelReservation, [reservationId])
		return information.affectedRows
	}
}