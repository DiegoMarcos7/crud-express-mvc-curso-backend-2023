import StoreProcedures from "../db/StoreProcedures"
import ModelBase from "./ModelBase"
import PaymentMethodQE from "./query-entity/PaymentMethodQE"

export default class PaymentMethodModel extends ModelBase{
	async getPaymentMethods() : Promise<PaymentMethodQE[]> {
		const [[resultset]] = await this.database.query(StoreProcedures.getAllPaymentMethods)
        return resultset.map( (record : any) => new PaymentMethodQE(record))
	}
}