import fs from "fs/promises";

let storage;
async function connect() {
	storage = JSON.parse(
		await fs.readFile("./src/model/roomReservations.json", {
			encoding: "utf-8",
		})
	);
}
const db = {
	getReservations: () => storage.reservations,
	setReservations: newReservations => (storage.reservations = newReservations),
	connect,
};
export default db;