import CustomerRegisterDTO from "../controllers/dtos/CustomerRegisterDTO"
import UserRegisterDTO from "../controllers/dtos/UserRegisterDTO"
import StoreProcedures from "../db/StoreProcedures"
import ModelBase from "./ModelBase"
import UserQE from "./query-entity/UserQE"

export default class UserModel extends ModelBase{
	async registerCustomerUser({username, password}: UserRegisterDTO, {name, surname, email, phoneNumber, countryId}: CustomerRegisterDTO) : Promise<number>{
		const [information] = 
			await this.database.query(StoreProcedures.registerCustomerUser, [
				username, password, 
				name, surname, email, phoneNumber, countryId])
		return information.affectedRows
	}
	
	async findPasswordByUsername (username : string) : Promise<UserQE> {
		const [[resultset]] = 
			await this.database.query(StoreProcedures.findPasswordByUsername, [username])
        if(!Array.isArray(resultset) || resultset.length === 0) return null
		const [record] = resultset
        return new UserQE(record)
	}
}