import StoreProcedures from "../db/StoreProcedures"
import ModelBase from "./ModelBase"
import PaymentQE from "./query-entity/PaymentQE"

export default class PaymentModel extends ModelBase{
	async findReservationPayment(id) : Promise<PaymentQE> {
		const [[resultset]] = 
			await this.database.query(StoreProcedures.getReservationPaymentById, [id])
        if(!Array.isArray(resultset) || resultset.length === 0) return null
		const [record] = resultset
        return new PaymentQE(record)
	}
}