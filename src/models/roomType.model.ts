import StoreProcedures from "../db/StoreProcedures"
import ModelBase from "./ModelBase"
import RoomTypeQE from "./query-entity/RoomTypeQE"

export default class RoomTypeModel extends ModelBase{
	async getRoomTypes() : Promise<RoomTypeQE[]> {
		const [[resultset]] = 
			await this.database.query(StoreProcedures.getAllRoomTypes) as [any[]]
		return resultset.map( (record : any) => new RoomTypeQE(record) )
	}
}