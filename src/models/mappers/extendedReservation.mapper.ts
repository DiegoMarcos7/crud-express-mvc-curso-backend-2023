import { ExtendedReservation } from "../../entities/Reservation"

export default function extendedReservationMapper (resultset : any[]) : ExtendedReservation{
    const [header] = resultset
	return {
		id: header["reservation_id"],
		checkIn: header["check_in"],
		checkOut: header["check_out"],
		reservationDate: header["reservation_date"],
		customer: {
			id: header["customer_id"],
			name: header["name"],
			surname: header["surname"],
			email: header["email"],
			phoneNumber: header["phone_number"],
			countryId: header["country_id"],
		}
	}
}