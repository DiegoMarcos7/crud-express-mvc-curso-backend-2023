import Payment from "../../entities/Payment"
import PaymentMethod from "../../entities/PaymentMethod"

export default class PaymentQE {
    readonly id : number
    readonly reservationId : number
    readonly paymentMethod : PaymentMethod
    readonly paymentDate : Date
    constructor(record : any) {
        this.id = record["payment_id"]
        this.reservationId = record["reservation_id"]
        this.paymentMethod = {
            id : record["payment_method_id"],
            name : record["name"]
        }
        this.paymentDate = record["payment_date"]
    }
    toEntityDomain(): Payment{
        return { ...this }
    }
}