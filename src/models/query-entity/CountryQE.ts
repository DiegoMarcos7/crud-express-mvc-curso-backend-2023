import Country from "../../entities/Country"

export default class CountryQE {
    readonly id : number
    readonly name : string
    constructor(record : any) {
        this.id = record["id"]
        this.name = record["name"]
    }
    toEntityDomain(): Country{
        return { ...this }
    }
}