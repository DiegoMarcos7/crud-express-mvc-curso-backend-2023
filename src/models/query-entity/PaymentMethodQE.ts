import PaymentMethod from "../../entities/PaymentMethod"

export default class PaymentMethodQE {
    readonly id : Number
    readonly name : string
    constructor(record : any) {
        this.id = record["id"]
        this.name = record["name"]
    }
    toEntityDomain(): PaymentMethod{
        return { ...this }
    }
}