import RoomType from "../../entities/RoomType"

export default class RoomTypeQE {
    readonly id : number
    readonly name : string
    readonly capacity : number
    readonly nightlyPrice : number
    readonly characteristics : string
    constructor(record: any) {
        this.id = record["id"],
        this.name = record["name"],
        this.capacity = record["capacity"],
        this.nightlyPrice = record["nightly_price"],
        this.characteristics = record["characteristics"]
    }
    toEntityDomain(): RoomType{
        return { ...this }
    }
}