import Customer from "../../entities/Customer"

export default class CustomerQE {
    readonly id : number
    readonly name : string
    readonly surname : string
    readonly email : string
    readonly phoneNumber : string
    readonly countryId : string
    constructor(record : any) {
        this.id = record["id"]
        this.name = record["name"]
        this.surname = record["surname"]
        this.email = record["email"]
        this.phoneNumber = record["phone_number"]
        this.countryId = record["country_id"]
    }
    toEntityDomain(): Customer{
        return { ...this }
    }
}