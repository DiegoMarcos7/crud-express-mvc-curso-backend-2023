import Room from "../../entities/Room"
import RoomType from "../../entities/RoomType"

export default class RoomQE {
    readonly id : number
    readonly roomType : RoomType
    constructor(record : any) {
        this.id = record["room_id"]
        this.roomType = {
            id : record["room_type_id"],
            name : record["name"],
            capacity : record["capacity"],
            nightlyPrice : record["nightly_price"],
            characteristics : record["characteristics"]
        }
        // this.status = Boolean(Buffer.from(record["status"]).readInt8())
    }
    toEntityDomain(): Room{
        return { ...this }
    }
}