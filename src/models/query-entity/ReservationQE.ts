import Reservation from "../../entities/Reservation"

export default class ReservationQE {
    readonly id : number
    readonly customerId : number
    readonly checkIn : Date
    readonly checkOut : Date
    readonly reservationDate : Date
    constructor(record: any) {
        this.id = record["id"],
        this.customerId = record["customer_id"],
        this.checkIn = record["check_in"],
        this.checkOut = record["check_out"],
        this.reservationDate = record["reservation_date"]
    }
    toEntityDomain(): Reservation{
        return { ...this }
    }
}