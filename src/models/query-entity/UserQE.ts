import User from "../../entities/User"

export default class UserQE {
    readonly id : number 
    readonly username : string
    readonly passwordHashed : string
    constructor(record : any) {
        this.id = record["id"]
        this.username = record['username']
        this.passwordHashed = record['password']
    }
    toEntityDomain(): User{
        return { 
            id :this.id, 
            username: this.username
        }
    }
}