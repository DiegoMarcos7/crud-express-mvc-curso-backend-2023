import StoreProcedures from "../db/StoreProcedures"
import ModelBase from "./ModelBase"
import CustomerQE from "./query-entity/CustomerQE"

export default class CustomerModel extends ModelBase {
    async getCustomers() : Promise<CustomerQE[]> {
		const [[resultset]] = 
			await this.database.query(StoreProcedures.getAllCustomers) as [any[]]
			
		return resultset.map( (record : any) => new CustomerQE(record) )
	}
	async findCustomer(id : number) : Promise<CustomerQE> {
		const [[resultset]] = await this.database.query(StoreProcedures.findCustomerById, [id])
		if(!Array.isArray(resultset) || resultset.length === 0) return null
		const [record] = resultset
		return new CustomerQE(record)
	}
}