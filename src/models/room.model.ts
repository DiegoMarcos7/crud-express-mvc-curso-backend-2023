import StoreProcedures from "../db/StoreProcedures"
import ModelBase from "./ModelBase"
import RoomQE from "./query-entity/RoomQE"

export default class RoomModel extends ModelBase{
	async getAvailableRooms() : Promise<RoomQE[]> {
		const [[resultset]] = await this.database.query(StoreProcedures.getAllAvailableRooms)
        return resultset.map( (record : any) => new RoomQE(record) )
	}
	async findAvailableRoom(id : string) : Promise<RoomQE> {
		const [[resultset]] = 
			await this.database.query(StoreProcedures.findRoomById,[id]) as [any[]]
		if(!Array.isArray(resultset) || resultset.length === 0) return null
		const [record] = resultset
		return new RoomQE(record)
	}
}