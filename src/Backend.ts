import express, { Application } from "express"
import dotenv from 'dotenv'
import ControllerInitializer from "./controllers"
import Encryptor from "./utils/encryption"
export default class Backend {
    private app: Application
    constructor() {
        // Initializations
        this.app = express()
    }
    setup(){
        this.setupDotenv()
        this.setupJson()
        this.setupControllers()
    }
    private setupJson(){
        this.app.disable('x-powered-by')
        this.app.use(express.json())
    }
    //await db.connect()

    private setupControllers(){
        /* const controllers = [
            new ReservationController()
            ]
            controllers.forEach(({root, router}) => app.use(root, router)) */
        //usando controller
        /* let reservationController = new ReservationController()
        app.use(reservationController.root, reservationController.router) */
        
        new ControllerInitializer(this.app).init()
    }
    private setupDotenv(){
        dotenv.config()
        Encryptor.init()
    }
    start(){
        const PORT = process.env.PORT || 3000
        this.app.listen(PORT, () => {
            console.log(`Server listen on port ${PORT}`)
        })
    }
}