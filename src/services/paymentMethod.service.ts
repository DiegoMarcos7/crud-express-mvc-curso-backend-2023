import PaymentMethod from "../entities/PaymentMethod"
import PaymentMethodModel from "../models/paymentMethod.model"

export default class PaymentMethodService {
    private paymentMethodModel: PaymentMethodModel
	constructor() {
		this.paymentMethodModel = new PaymentMethodModel()
	}
	async getPaymentMethods(): Promise<PaymentMethod[]> {
		const paymentMethodQEList = 
			await this.paymentMethodModel.getPaymentMethods()
		return paymentMethodQEList.map( record => record.toEntityDomain())
	}
}