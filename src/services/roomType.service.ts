import RoomType from "../entities/RoomType"
import RoomTypeQE from "../models/query-entity/RoomTypeQE"
import RoomTypeModel from "../models/roomType.model"

export default class RoomTypeService {
    private roomTypeModel: RoomTypeModel
	constructor() {
		this.roomTypeModel = new RoomTypeModel()
	}
	async getRoomTypes(): Promise<RoomType[]> {
		const roomTypeQEList : RoomTypeQE[] = await this.roomTypeModel.getRoomTypes()
		return roomTypeQEList.map( record => record.toEntityDomain() )
	}
}