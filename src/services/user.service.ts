import CustomerRegisterDTO from "../controllers/dtos/CustomerRegisterDTO"
import UserRegisterDTO from "../controllers/dtos/UserRegisterDTO"
import UserQE from "../models/query-entity/UserQE"
import UserModel from "../models/user.model"
import Encryptor from "../utils/encryption"
import jwt from "jsonwebtoken"
export default class UserService {
    private userModel: UserModel
	constructor() {
		this.userModel = new UserModel()
	}

	async registerCustomerUser(user : UserRegisterDTO, customer : CustomerRegisterDTO) : Promise<boolean> {
		const hashedPassword = await Encryptor.encrypt(user.password)
		const affectedRows : number = 
			await this.userModel.registerCustomerUser({username: user.username, password: hashedPassword}, customer)
		return affectedRows > 0
	}

	async findPasswordByUsername(username : string, password : string): Promise<string> {
        const userQE : UserQE = await this.userModel.findPasswordByUsername(username)
		if (userQE) {
			const isPasswordValid = await Encryptor.compare(password, userQE.passwordHashed)
			if (isPasswordValid)
				return jwt.sign(userQE.toEntityDomain(), process.env.JWT_PRIVATE_KEY)
		}
		return null
	}
}