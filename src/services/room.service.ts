import Room from "../entities/Room"
import RoomModel from "../models/room.model"

export default class RoomService {
    private roomModel: RoomModel
	constructor() {
		this.roomModel = new RoomModel()
	}
	async getAvailableRooms(): Promise<Room[]> {
		const roomQEList = 
			await this.roomModel.getAvailableRooms()
		return roomQEList.map( record => record.toEntityDomain() )
	}
	async findAvailableRoom(id : string) : Promise<Room>{
		const roomQE = await this.roomModel.findAvailableRoom(id)
		if (!roomQE) return null
		return roomQE.toEntityDomain()
	}
}