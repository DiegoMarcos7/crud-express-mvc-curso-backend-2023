import Country from "../entities/Country"
import CountryModel from "../models/country.model"

export default class CountryService {
    private countryModel: CountryModel
	constructor() {
		this.countryModel = new CountryModel()
	}
	async getCountries(): Promise<Country[]> {
		const countryQEList = 
			await this.countryModel.getCountries()
		return countryQEList.map( record => record.toEntityDomain() )
	}
}