import ReservationCreateDTO from "../controllers/dtos/ReservationCreateDTO"
import ReservationUpdateDTO from "../controllers/dtos/ReservationUpdateDTO"
import Reservation, { ExtendedReservation } from "../entities/Reservation"
import ReservationQE from "../models/query-entity/ReservationQE"
import ReservationModel from "../models/reservation.model"

export default class ReservationService {
	private reservationModel: ReservationModel
	constructor() {
		this.reservationModel = new ReservationModel()
	}
	async getReservations(): Promise<Reservation[]> {
		const reservationQEList : ReservationQE[] =  await this.reservationModel.getReservations()
		return reservationQEList.map( record => record.toEntityDomain() )
	}
	async findReservation(reservationId : number): Promise<ExtendedReservation> {
		return await this.reservationModel.findReservation(reservationId)
	}
	async createReservation(reservation: ReservationCreateDTO): Promise<boolean> {
		const affectedRows : number = await this.reservationModel.createReservation(reservation)
		return affectedRows > 0
	}
	async editReservation(reservation: ReservationUpdateDTO): Promise<boolean> {
		const affectedRows = await this.reservationModel.editReservation(reservation)
		return affectedRows > 0
	}
	async cancelReservation(reservationId : number): Promise<boolean> {
		const affectedRows = await this.reservationModel.cancelReservation(reservationId)
		return affectedRows > 0
	}
}