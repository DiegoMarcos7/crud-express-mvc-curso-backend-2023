import Payment from "../entities/Payment"
import PaymentModel from "../models/payment.model"
import PaymentQE from "../models/query-entity/PaymentQE"

export default class PaymentService {
    private paymentModel: PaymentModel
	constructor() {
		this.paymentModel = new PaymentModel()
	}
	async findReservationPayment(id): Promise<Payment> {
		const paymentQE : PaymentQE = 
			await this.paymentModel.findReservationPayment(id)
        if (!paymentQE) return null
		return paymentQE.toEntityDomain()
	}
}