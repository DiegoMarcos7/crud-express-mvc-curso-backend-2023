import Customer from "../entities/Customer"
import CustomerModel from "../models/customer.model"
import CustomerQE from "../models/query-entity/CustomerQE"

export default class CustomerService {
    private customerModel: CustomerModel
	constructor() {
		this.customerModel = new CustomerModel()
	}
	async getCustomers(): Promise<Customer[]> {
		const customerQEList = 
			await this.customerModel.getCustomers()
		return customerQEList.map( record => record.toEntityDomain() )
	}
	async findCustomer( id : number) {
		const customerQE : CustomerQE = await this.customerModel.findCustomer(id)
		if (!customerQE) return null
		return customerQE.toEntityDomain()
	}
}