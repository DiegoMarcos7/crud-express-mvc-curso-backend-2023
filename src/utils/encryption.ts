import bcrypt from "bcrypt"

export default class Encryptor {
    private static ENCRYPT_PEPPER : string
    static init() {
        Encryptor.ENCRYPT_PEPPER = process.env.ENCRYPT_PEPPER
    }

    private static withPepper(text:string) {
        return `${text}${Encryptor.ENCRYPT_PEPPER}`
    }

    static async encrypt(plaintext : string){
        return await bcrypt.hash(Encryptor.withPepper(plaintext), 12)
    }

    static async compare(text : string, hashedText : string) {
        return await bcrypt.compare(Encryptor.withPepper(text), hashedText)
    }
}