const countries = [
    'PE',
    'US',
    'BR',
    'AR',
    'CL',
    'CO',
    'EC',
    'ES',
    'FR',
    'IT',
    'DE',
    'UK',
]

export function isDateValid(dateString: string){
    return !isNaN(new Date(dateString).getTime())
}
export function isNumber(value : any){
    return !Number.isNaN(parseInt(value))
}
export function isUsername(value : string){
    const regexPattern = /^[a-zA-Z0-9]{6,15}$/
    return regexPattern.test(value)
}
export function isPassword(value : string){
    const regexPattern = /^[a-zA-Z0-9]*$/
    return regexPattern.test(value)
}
export function isNameOrSurname(value : string){
    const regexPattern = /^[a-zA-Z]{1,50}$/
    return regexPattern.test(value)
}
export function isEmail(value : string){
    const regexPattern = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}/
    return regexPattern.test(value)
}
export function isPhoneNumber(value : string){
    const regexPattern = /^\d{9}$/
    return regexPattern.test(value)
}
export function isCountryId(value : string){
    return countries.includes(value)
}