# CRUD-express-MVC-curso-backend-2023

El proyecto es un CRUD de reservas de un hotel
- Se usa el framework para aplicaciones web Express.js.
- Se construye con la arquitectura MVC 

:smiley:

## Endpoints de la RestAPI

**RESERVATION**

1. **Obtener todas las reservaciones realizadas en un intervalo de fecha**: 
    
    `GET /api/reservations`, este endpoint para interfaz administrativo.
    
    **Response**
    ```json
    {
    	"message" : "SUCCESS",
    	"data" : [
    		{
    			"id" : number,
                "customer": number,
                "checkIn": Datetime, // formato : '2023-10-01 10:00:00'
                "checkOut": Datetime, // formato : '2023-10-03 12:30:00'
                "reservationDate": Datetime, // formato : '2023-09-29 12:00:00'
    		}
    	]
    }
    ```
    
2. **Obtener una reserva por ID**:
    
    `GET /api/reservations/{id}`

    **Response**
    ```json
    {
    	"message": "SUCCESS",
    	"data": {
            "customer": {
    		  "id": number, 
    		  "name" : string,
    		  "surname" : string, 
    		  "email" : string, 
    		  "phoneNumber" : string, 
    		  "countryId" : string
    		},
    		"checkIn" : Datetime, // formato : '2023-10-01 10:00:00'
            "checkOut" : Datetime, // formato : '2023-10-03 12:30:00'
            "reservationDate" : Datetime
    	}
    }
    ```
    
3. **Crear una nueva reserva**:
    
    `POST /api/reservations/register`, para interfaz de usuario.
    
    **Request**
    ```json
    {
        "customerId" : 6 number,
        "checkIn" : "2023-11-10 15:00:00" Datetime, 
        "checkOut" : "2023-11-15 12:00:00" Datetime, 
    }
    ```
    **Response**
     ```json
    {
    	"message": "SUCCESS_RESERVATION_CREATED",
    	"data" : null
    }
    ```
    
4. **Actualizar la fecha de reservación**:
    
    `PATCH /api/reservations/change-calendar/{id}`, para interfaz de usuario.
    
    **Request**
    ```json
    {
    	"checkIn": "2023-12-11 9:00:00" Datetime,
        "checkOut": "2023-12-15 10:00:00" Datetime
    }
    ```
    **Response**
    ```json
    {
    	"message" : "SUCCESS_RESERVATION_DATE_UPDATED",
    	"data": null
    }
    ```
    
5. **Cancelar una reserva**:
    
    `DELETE /api/reservations/cancel/{id}`, para interfaz administrativo.
    
    **Response**
     ```json
    {
    	"message": "SUCCESS_RESERVATION_CANCELED",
    	"data": null
    }
    ```
6. **Obtener una lista de reservas de un cliente**:
    
    `GET /api/reservations/customer/{customerId}`, para la interfaz de cliente y la app de administración.
    
    **Response**
     ```json
    {
    	"message": "SUCCESS",
    	"data": [ 
            {
    			"reservationNumber" : number,
    			"checkIn": Datetime, // formato : '2023-10-01 10:00:00'
    		    "checkOut": Datetime, // formato : '2023-10-03 12:30:00'
    			"reservationDate" : Datetime, // formato : '2023-09-29 12:00:00'
    			"roomId": char(2) 
    		}
    	]
    }
    ```

**ROOM**

1. **Obtener todas las habitaciones disponibles para ocupar**:
    
    `GET /api/rooms/available`
    
    **Response**
     ```json
    {
    	"message": "SUCCESS",
    	"data": [
    		{
    			"roomId" : char(2), 
    			"roomType" : RoomType
    		}
    	]
    }
    ```
    
2. **Obtener detalles de una habitación por ID**:
    
    `GET /api/rooms/{id}`
    
    **Response**
     ```json
    {
        "message": "SUCCESS",
        "data": {
    	    "roomId" : char(2), 
    	    "roomType" : RoomType {
                "id" : number,
                "name" : string,
                "capacity" : number,
                "nightlyPrice" : number,
                "characteristics" : string
    	    }
        }
    }
    ```
    

**ROOM TYPES**

1. **Obtener una lista de tipos de habitaciones**:
    
    `GET /api/room-types`, todas las habitaciones que muestro, deben estar disponibles.
    
    **Response**
     ```json
    {
    	"message": "SUCCESS",
    	"data": [{
            "roomTypeId" : number,
            "name" : string,
            "capacity" : number,
            "nightlyPrice" : number,
            "characteristics" : string
        }]
    }
    ```
    

**CUSTOMER**

1. **Obtener una lista de un clientes**:
    
    `GET /api/customers`, la interfaz de administración.
    
    **Response**
     ```json
    {
    	"message": "SUCCESS",
    	"data": [
    		{
    		  "id": number, 
    		  "name" : string,
    		  "surname" : string, 
    		  "email" : string, 
    		  "phoneNumber" : string, 
    		  "countryId" : string
    		}
    	]
    }
    ```
    
2. Encontrar un cliente por id
    
    `GET /api/customers/{id}`, este endpoint para la interfaz de cuenta de cliente.
    
    **Response**
     ```json
    {
    	"message": "SUCCESS",
    	"data": {
    		"id": number, 
            "name" : string,
            "surname" : string, 
            "email" : string, 
            "phoneNumber" : string, 
            "countryId" : string
    	}
    }
    ```
    
3. Editar un cliente
    
    `PUT /api/customers/edit/{id}`, para el perfil del cliente. 
    
    **Request**
    ```json
    {   
        "name" : "Emily" string,
        "surname" : "Brown" string,  
        "email" : "eBrown@example.com" string, 
        "phoneNumber" : "985231473" string, 
        "countryId" : "UK" string
    }
    ```
    **Response**
     ```json
    {
    	"message" : "SUCCESS_CUSTOMER_UPDATED",
    	"data" : null
    }
    ```

**PAYMENT METHOD**

1. Obtener métodos de pago disponibles:
    
    `GET /api/payment-methods`
    
    **Response**
     ```json
    {
    	"message": "SUCCESS",
    	"data": [
    		{
    			"paymentMethodId": number,
    			"name" : string
    		}
    	]
    }
    ```
    

**COUNTRY**

1. **Obtener una lista de países disponibles**:
    
    `GET /api/countries`
    
    **Response**
     ```json
    {
    	"message": "SUCCESS",
    	"data": [
    		{
    			"countryId": char(2),
    			"name" : string
    		}
    	]
    }
    ```
    

**RESERVATION - PAYMENT**

1. **Obtener información sobre el pago de una reserva**:
    
    `GET /api/reservation-payments/{reservationId}`, para la interfaz del administrador.
    
    **Response**
     ```json
    {
    	"message": "SUCCESS",
    	"data": {
    		"paymentId" : number,
    		"reservationId" : number, 
    		"paymentMethod": paymentMethod , 
    		"paymentDate" : datetime // formato : '2023-10-15 12:30:00'
    	} 
    }
    ```
    
2. **Realizar un pago por una reserva**:
    
     `POST /api/reservation-payments/register`, para la interfaz de pago.
    
    **Request**
    ```json
    {
    	"reservationId" : 1 number,
    	"paymentMethod" : 2 number, 
    	"paymentDate" : "2023-09-29 12:00:00" datetime 
    }
    ```
    **Response**
    ```json
    {
    	"message": "SUCCESS_PAYMENT_REGISTERED",
    	"data" : null
    }
    ```

**USER** 
Endpoint para la interfaz de autenticación y registro del usuario.

1. Autorizar el acceso de un user-customer
    
    `POST /api/auth/sing-in` 
    
    **Request**
    ```json
    {
    	"user": "mVera" string,
    	"password": "mVera123" string
    }
    ```	
    **Response**
     ```json
    {
    	"message" : "SUCCESS_USER_AUTHENTICATED",
    	"data": null
    }
    ```
    
2. Registrar un user-customer
    
    `POST /api/auth/sign-up` 
    
    **Request**
    ```json
    {
    	"user": "mVera" string,
    	"password": "mVera123" string,
    	"name" : "Martin" string,
        "surname" : "Vera" string, 
        "email" : "martin.vera@example.com" string, 
        "phoneNumber" : "985475236" string, 
        "countryId" : "PE" string
    }
    ```
    **Response**
    ```json
    {
    	"message" : "SUCCESS_USER_REGISTERED",
    	"data" : null
    }
    ```
    
    Nota: Cada vez que realiza el registro de un user customer, tengo que registrar los datos en la tabla customer.

3. Delete cliente
    
    Eliminar información de un cliente este va relacionado con el usuario, ya que al eliminar un cliente el usuario ya no existe.
    
    `DELETE /api/customers/delete/{id}`
    
    **Response**
     ```json
    {
    	"message": "SUCCESS_CUSTOMER_DELETED",
    	"data" : null
    }
    ```